package adjustamat.infodisplay.data;
/**
 * An info string.
 */
public class Line{
   private String text = "";
   private final Display parent;

   Line(String text, Display parent){
      this.text = text;
      this.parent = parent;
   }

   Line(Display d){
      parent = d;
   }

   public String getText(){
      return text;
   }

   public void setText(String text){
      this.text = text;
   }
}
