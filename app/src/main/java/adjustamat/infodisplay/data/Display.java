package adjustamat.infodisplay.data;
import android.app.Fragment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import adjustamat.infodisplay.R;

/**
 * An ordered list of lines with search capabilities.
 */
public class Display{
   public static final List<Display> displayList = new ArrayList<>(10);
   public static final Map<String, LinkedList<Display>> savedTitles = new HashMap<>();
   public static final Map<String, LinkedList<Line>> savedLines = new HashMap<>();
   public static final Map<Integer, Display> savedIDs = new HashMap<>();
   private static int maxID;

   public static Display addDisplay(){
      Display d = new Display();
      displayList.add(d);
      savedIDs.put(d.id, d);
      return d;
   }

   private static Display addDisplay(String t){
      Display d = new Display();
      d.setTitle(t);
      displayList.add(d);
      savedIDs.put(d.id, d);
      return d;
   }

   static{
      // TODO: load from database.
      maxID = 0;
      Display d;
      d = addDisplay("Example 1");
      d.getLine(0).setText("Display 1 Line 1");
      d.addLine("Display 1 Line 2");
      d = addDisplay("Example 2");
      d.getLine(0).setText("Display 2 Line 1");
      d.addLine("Display 2 Line 2");
      d.addLine("Display 2 Line 3");
      d = addDisplay("Example 3");
      d.getLine(0).setText("Display 3 Line 1");
      d.addLine("Display 3 Line 2");
      d.addLine("Display 3 Line 3");
      d.addLine("Display 3 Line 4");
   }

   //   private LinkedList<Display> savedTitleMapParent = null;
   public final int id;
   private String title = "";
   public final ArrayList<Line> lineList = new ArrayList<>(1);

   private Display(){
      id = maxID++;
      addLine();
   }

   // TODO: map titles to Displays for search function in Navdrawer.
   public String getTitle(){
      return title;
   }

   public CharSequence getTitleOrUntitled(Fragment f){
      if(title.length() == 0){
         return f.getText(R.string.untitled);
      }
      return title;
   }

   public Line addLine(){
      Line ret = new Line(this);
      lineList.add(ret);
      return ret;
   }

   private void addLine(String s){
      Line ret = new Line(s, this);
      lineList.add(ret);
   }

   public Line getLine(int i){
      return lineList.get(i);
   }

   public void setTitle(String title){
      if(title == null){
         title = "";
      }
      if(this.title.length() > 0){
         LinkedList<Display> titleMapParent = savedTitles.remove(this.title);
         titleMapParent.remove(this);
         savedTitles.put(this.title, titleMapParent);
      }
      if(title.length() > 0){
         LinkedList<Display> titleMapParent = savedTitles.remove(title);
         if(titleMapParent == null){
            titleMapParent = new LinkedList<>();
         }
         titleMapParent.add(this);
         savedTitles.put(title, titleMapParent);
      }
      this.title = title;
   }

   @Override
   public String toString(){
      return title;
   }
}
