package adjustamat.infodisplay;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;

/**
 * Item in ActCreator list, ActCreator-Orderer list.
 */
public class ItemDisplay extends LinearLayout{
   public ItemDisplay(Context context){
      this(context, null, 0);
   }

   public ItemDisplay(Context context, AttributeSet attrs){
      this(context, attrs, 0);
   }

   public ItemDisplay(Context context, AttributeSet attrs, int defStyleAttr){
      super(context, attrs, defStyleAttr);
      //TODO: init //return inflater.inflate(R.layout.item_display, container, false);
      // TODO: see attrs_item_display.xml
   }
/*
<adjustamat.infodisplay.CreatorComponent xmlns:android="http://schemas.android.com/apk/res/android"
                                         android:layout_width="match_parent"
                                         android:layout_height="match_parent"
                                         android:orientation="vertical">
   <EditText android:layout_width="match_parent"
             android:layout_height="wrap_content"
             android:id="@+id/txtText"
             android:layout_alignParentTop="true"
             android:layout_alignParentStart="true"
             android:layout_alignParentLeft="true"
             android:inputType="text"
             android:hint="@string/txtText"/>
   <LinearLayout android:layout_width="match_parent"
                 android:layout_height="wrap_content">
      <Button android:layout_width="wrap_content"
              android:layout_height="wrap_content"
              android:text="@string/btnFont"
              android:id="@+id/btnFont"
              style="?android:attr/buttonStyleSmall"
              android:onClick="onBtnFontClick"/>
      <Button style="?android:attr/buttonStyleSmall"
              android:layout_width="wrap_content"
              android:layout_height="wrap_content"
              android:text="@string/btnColor"
              android:id="@+id/btnColor"
              android:onClick="onBtnColorClick"/>
      <Button style="?android:attr/buttonStyleSmall"
              android:layout_width="wrap_content"
              android:layout_height="wrap_content"
              android:text="@string/btnBackground"
              android:id="@+id/btnBackground"
              android:onClick="onBtnBackgroundClick"/>
   </LinearLayout>
</adjustamat.infodisplay.CreatorComponent>
 */
}
