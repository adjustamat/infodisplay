package adjustamat.infodisplay;
import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.TextView;

import adjustamat.infodisplay.activity.ActCreator;
import adjustamat.infodisplay.activity.ActDummy;
import adjustamat.infodisplay.activity.ActFullscreen;
import adjustamat.infodisplay.activity.ActSettings;
import adjustamat.infodisplay.data.Display;

/**
 * Fragment used for managing interactions for and presentation of a navigation drawer.
 * See the <a href="https://developer.android.com/design/patterns/navigation-drawer.html#Interaction">
 * design guidelines</a> for a complete explanation of the behaviors implemented here.
 */
public class Navdrawer extends Fragment implements ExpandableListView.OnChildClickListener{
   private static final String intArgument = "adjustamaton.int";
   private ExpandableListView lists;
   private int selectedGroup = 0;
   private static final String groupPosKey = "selectedGroup";
   private int selectedChild = 0;
   private static final String childPosKey = "selectedChild";
   private DrawerLayout top;
   private View me;
   private ActionBarDrawerToggle v7abdt;
   private boolean fullscreen;
//   private boolean navigated=true;

   //   private boolean tmpNotFirstCreate; // first
//   // flag indicating whether or not the user has demonstrated awareness of this feature:
//   private boolean userLearnedDrawer; // userAware
//   private static final String userLearnedDrawerKey = "userLearnedDrawer";
//   public void onCreateOptionsMenu(Menu menu, MenuInflater inflater){
//      // If the drawer is open, show the global app actions in the action bar. See also
//      // showGlobalContextActionBar, which controls the top-left area of the action bar.
//      if(top != null && top.isDrawerOpen(me)){
//         inflater.inflate(R.menu.main, menu);
//      }
//      super.onCreateOptionsMenu(menu, inflater);
//   }
//   public boolean onOptionsItemSelected(MenuItem item){
//      if(v7abdt.onOptionsItemSelected(item)){
//         return true;
//      }
//      switch(item.getItemId()){
//         case R.id.action_example:
//            example(getActivity());
//            return false;
//         case R.id.action_settings:
//            showSettings(getActivity());
//            return false;
//      }
   public Navdrawer(){
      // Indicate that this fragment would like to influence the set of actions in the action bar.
      setHasOptionsMenu(true);
   }

   /*
     > onAttach(Activity) called once the fragment is associated with its activity.
     > onCreate(Bundle) called to do initial creation of the fragment.
     > onCreateView(LayoutInflater, ViewGroup, Bundle) creates and returns the view hierarchy associated with the fragment.
     > onActivityCreated(Bundle) tells the fragment that its activity has completed its own Activity.onCreate().
       onViewStateRestored(Bundle) tells the fragment that all of the saved state of its view hierarchy has been restored.
       onStart() makes the fragment visible to the user (based on its containing activity being started).
       onResume() makes the fragment interacting with the user (based on its containing activity being resumed).
   ===============================================================================================
       onPause() fragment is no longer interacting with the user either because its activity is being paused
                 or a fragment operation is modifying it in the activity.
       onStop() fragment is no longer visible to the user either because its activity is being stopped
                or a fragment operation is modifying it in the activity.
       onDestroyView() allows the fragment to clean up resources associated with its View.
       onDestroy() called to do final cleanup of the fragment's state.
     > onDetach() called immediately prior to the fragment no longer being associated with its activity.
    */
   @Override
   public void onDetach(){
      super.onDetach();
   }

   @Override
   public void onAttach(Activity activity){
      super.onAttach(activity);
//      inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
      fullscreen = activity instanceof ActFullscreen;
   }

   /**
    * Create the graphics of the Navdrawer. Is this done before adding it to an activity?
    * @param inflater           inflater
    * @param container          container
    * @param savedInstanceState Bundle savedInstanceState
    * @return ListView navdrawer
    */
   @Override
   public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
      lists = (ExpandableListView)inflater.inflate(R.layout.navdrawer, container, false);
      lists.setAdapter(new ListAdapt(inflater));
      lists.setOnChildClickListener(this);
// put SearchView between Group 0 and Group 1 in ExpandableListView if that works, otherwise split into two ExpandableListViews.
      return lists;
   }

   @Override
   public void onSaveInstanceState(Bundle outState){
      super.onSaveInstanceState(outState);
      outState.putInt(groupPosKey, selectedGroup);
   }

   @Override
   /**
    * Load saved instance state, Initialize the navigation drawer.
    *
    * WARNINGS when selecting Settings in menu.actions:
    * W/art﹕ Before Android 4.1, method int android.support.v7.internal.widget.ListViewCompat.lookForSelectablePosition(int, boolean)
    * would have incorrectly overridden the package-private method in android.widget.ListView
    * W/InputEventReceiver﹕ Attempted to finish an input event but the input event receiver has already been disposed.
    */
   public void onActivityCreated(Bundle savedInstanceState){
      super.onActivityCreated(savedInstanceState);
      // The DrawerLayout containing this drawer.
      top = (DrawerLayout)getActivity().findViewById(R.id.layout_top);
      // The android:id of this drawer's <fragment/> in the calling activity.
      me = getActivity().findViewById(R.id.navdrawer);
      if(fullscreen){
         //TODO: I think I have to use only one Activity and make it fullscreen when it has fragment.Fullscreen
         /*
// update the main content by replacing fragments
        Fragment fragment = PlanetFragment.newInstance(position);

        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction ft = fragmentManager.beginTransaction();
        ft.replace(R.id.content_frame, fragment);
        ft.commit();

        // update selected item title, then close the drawer
        setTitle(mPlanetTitles[position]);
        mDrawerLayout.closeDrawer(mDrawerList);
          */
         v7abdt = null;
      }else{
         final ActionBarActivity act = (ActionBarActivity)getActivity();
         ActionBar actionBar = act.getSupportActionBar();
//         actionBar.setDisplayHomeAsUpEnabled(true);
         actionBar.setHomeButtonEnabled(true);
         // v7abdt ties together the the proper interactions between the navigation drawer and the action bar app icon.
         v7abdt = new ActionBarDrawerToggle(act, top, R.string.navdrawer_open, R.string.navdrawer_close){
            public void onDrawerClosed(View view){
               act.invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            public void onDrawerOpened(View drawerView){
               act.invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
         };
         // Defer code dependent on restoration of previous instance state.
         top.post(new Runnable(){
            public void run(){
               v7abdt.syncState();
            }
         });
         top.setDrawerListener(v7abdt);
      }
      // If the user hasn't 'learned' about the drawer, open it to introduce them to the drawer,
      // per the navigation drawer design guidelines.
//      if(!userLearnedDrawer && !tmpNotFirstCreate){
//         this.top.openDrawer(me);
//      }

      if(savedInstanceState != null){
         selectedGroup = savedInstanceState.getInt(groupPosKey);
         selectedChild = savedInstanceState.getInt(childPosKey);
//         tmpNotFirstCreate = true;
      }else{
         selectedGroup = 0;
         selectedChild = 0;
      }
      select();
      lists.expandGroup(0); // TODO: instance state - collapseGroup
      lists.expandGroup(1);
//      SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getActivity());
//      userLearnedDrawer = sp.getBoolean(userLearnedDrawerKey, false);
   }
/* Called whenever we call invalidateOptionsMenu() */
//    @Override
//    public boolean onPrepareOptionsMenu(Menu menu) {
//        // If the nav drawer is open, hide action items related to the content view
//        boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawerList);
//        menu.findItem(R.id.action_websearch).setVisible(!drawerOpen);
//        return super.onPrepareOptionsMenu(menu);
//    }
   private void select(){
      lists.setSelectedChild(selectedGroup, selectedChild, true);//TODO: select(selectedGroup, selectedChild);
      int item = (selectedGroup == 0 ?1 :STATIC_ID_COUNT) + selectedChild;
      lists.setItemChecked(item, true);
   }

   @Override
   public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long row){
//      navigate(groupPosition, childPosition);
//      return true; // click was handled.
//   }

//   private void navigate(int groupPosition, int childPosition){
//      navigated=true;
      if(selectedGroup == groupPosition){
         if(selectedChild == childPosition){
            return false;
         }/*else{
            // moving within a group to another child.
         }*/
      }/*else{
         // moving to another group.
      }*/
      selectedGroup = groupPosition;
      selectedChild = childPosition;
//      navigated=false;
      select();
//      if(top != null){
//         top.closeDrawer(me);
//      }
      if(groupPosition == 0){  // group 0: Act with fragments, ActSettings
         if(childPosition == 0){
            startActivity(new Intent(getActivity(), ActDummy.class));
         }else if(childPosition == 2){
            startActivity(new Intent(getActivity(), ActSettings.class));
         }else{
            startActivity(new Intent(getActivity(), ActCreator.class));
         }
      }else{ // group 1: ActFullscreen with Display.displayList
         startActivity(new Intent(getActivity(), ActFullscreen.class).putExtra(intArgument, childPosition));
      }
//      navigated=true;
      return true; // click was handled.
   }

   @Override
   public void onConfigurationChanged(Configuration newConfig){
      super.onConfigurationChanged(newConfig);
      v7abdt.onConfigurationChanged(newConfig);
   }

   private static final int GROUP_COUNT = 2;
   private static final int GROUP_0_SIZE = 3;
   private static final int STATIC_ID_COUNT = GROUP_COUNT + GROUP_0_SIZE;

   public class ListAdapt extends BaseExpandableListAdapter{
      private LayoutInflater inflater;

      ListAdapt(LayoutInflater inflater){
         this.inflater = inflater;
      }

      @Override
      public int getGroupCount(){
         return GROUP_COUNT;
      }

      @Override
      public int getChildrenCount(int groupPosition){
         return groupPosition == 0 ?GROUP_0_SIZE :Display.displayList.size();
      }

      @Override
      public Object getGroup(int groupPosition){
         return getGroupTitle(groupPosition);
      }

      //       0 (2) Act - Dummy
//       1 (3) Act - ActCreator+ActEditor
//       2 (4) ActSettings
//       (5...) ActFullscreen - displayList...
      @Override
      public Object getChild(int groupPosition, int childPosition){
         return getTitle(groupPosition, childPosition);
      }

      @Override
      public long getGroupId(int groupPosition){
         return groupPosition;
      }

      @Override
      public long getChildId(int groupPosition, int childPosition){
         return (groupPosition == 0 ?GROUP_COUNT :STATIC_ID_COUNT) + childPosition;
      }

      @Override
      public boolean hasStableIds(){
         return true;
      }

      @Override
      public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent){
         TextView v;
         if(convertView == null){
            // (isExpanded) ?mExpandedGroupLayout :mCollapsedGroupLayout
            v = (TextView)inflater.inflate(R.layout.item_nav, parent, false);
         }else{
            v = (TextView)convertView;
         }
         v.setText(getGroupTitle(groupPosition));
         return v;
      }

      @Override
      public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent){
         TextView v;
         if(convertView == null){
            v = (TextView)inflater.inflate(R.layout.item_nav, parent, false);
         }else{
            v = (TextView)convertView;
         }
         v.setText(getTitle(groupPosition, childPosition));
         return v;
      }

      public String getGroupTitle(int groupPosition){
         return groupPosition == 0 ?"Navigate" :"Display-list";
      }

      public CharSequence getTitle(int groupPosition, int childPosition){
         if(groupPosition == 0){
            return getNavTitle(childPosition);
         }
         return getDisplayTitle(childPosition);
      }

      public CharSequence getDisplayTitle(int i){
         return getDisplay(i).getTitleOrUntitled(Navdrawer.this);
      }

      public CharSequence getNavTitle(int i){
         switch(i){
            case 0:
               return getText(R.string.hello_world);// TODO: remove dummy, see values/strings.xml
            case 1:
               return getText(R.string.nav_creator_editor);//ActCreator.class;
            default:
               return getText(R.string.nav_settings);//ActSettings.class;
         }
      }

      public Display getDisplay(int i){
         return Display.displayList.get(i);
      }

      @Override
      public boolean isChildSelectable(int groupPosition, int childPosition){
         return true;
      }
   }
}