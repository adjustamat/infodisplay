package adjustamat.infodisplay.activity;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;

import adjustamat.infodisplay.R;

public class ActDummy extends ActionBarActivity{
   @Override
   protected void onCreate(Bundle savedInstanceState){
      super.onCreate(savedInstanceState);
      setContentView(R.layout.activity_dummy);
//      FragmentManager fragmentManager = getFragmentManager();
//      Navdrawer navdrawer = (Navdrawer)fragmentManager.findFragmentById(R.id.navdrawer);
//      navdrawer.init((DrawerLayout)findViewById(R.id.layout_top), R.id.navdrawer);
   }

   @Override
   public boolean onCreateOptionsMenu(Menu menu){
      getMenuInflater().inflate(R.menu.actions, menu);
      return true;
   }

//   @Override
//   public boolean onOptionsItemSelected(MenuItem item){
//      int id = item.getItemId();
//      if(id == R.id.action_nav_settings){
//         startActivity(new Intent(this, ActSettings.class));
//         return true;
//      }
//      return super.onOptionsItemSelected(item);
//   }
}
