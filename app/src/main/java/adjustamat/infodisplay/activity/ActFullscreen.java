package adjustamat.infodisplay.activity;

import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.view.View;

import adjustamat.infodisplay.R;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class ActFullscreen extends Activity{
   /**
    * If set, will toggle the system UI visibility upon interaction. Otherwise,
    * will show the system UI visibility upon interaction.
    */
   private boolean toggleOnClick = true;
   /**
    * Hide both the navigation bar and the status bar. TODO: ActionBar should be hidden by Theme_Fullscreen
    */
   public static final int FLAG_HIDE_ALL;

   static{
      int flag = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION/*2*/ | View.SYSTEM_UI_FLAG_LOW_PROFILE/*1*/;
      if(Build.VERSION.SDK_INT >= 16){
         flag |= View.SYSTEM_UI_FLAG_FULLSCREEN/*4*/;
         if(Build.VERSION.SDK_INT >= 19){
            flag |= View.SYSTEM_UI_FLAG_IMMERSIVE/*2048=0x800*/; // (IMMERSIVE_sticky/*4096=0x1000)*/
         }
      }
      FLAG_HIDE_ALL = flag;
   }

   @Override
   protected void onCreate(Bundle savedInstanceState){
      super.onCreate(savedInstanceState);
      setContentView(R.layout.activity_fullscreen);
      getWindow().getDecorView().setSystemUiVisibility(FLAG_HIDE_ALL);
//      final View controlsView = findViewById(R.id.layInputs);
//      final View contentView = findViewById(R.id.layTexts);

      // Set up an instance of SystemUiHider to control the system UI for
      // this activity.
//      mSystemUiHider = SystemUiHider.getInstance(this, contentView, SystemUiHider.FLAG_HIDE_NAVIGATION);
//      mSystemUiHider.setup();
//      mSystemUiHider.setOnVisibilityChangeListener(new SystemUiHider.OnVisibilityChangeListener(){
//         public void onVisibilityChange(boolean visible){
//            controlsView.setVisibility(visible ?View.VISIBLE :View.GONE);
//             if(visible && AUTO_HIDE){
//                // Schedule a hide().
//                delayedHide(AUTO_HIDE_DELAY_MILLIS);
//             }
//         }
//      });
      // Set up the user interaction to manually show or hide the system UI.
//      contentView.setOnClickListener(new View.OnClickListener(){
//         @Override
//         public void onClick(View view){
//            if(TOGGLE_ON_CLICK){
//               mSystemUiHider.toggle();
//            }else{
//               mSystemUiHider.show();
//            }
//         }
//      });
      // Upon interacting with UI controls, delay any scheduled hide()
      // operations to prevent the jarring behavior of controls going away
      // while interacting with the UI.
//      findViewById(R.id.dummy_button).setOnTouchListener(mDelayHideTouchListener);
   }

   @Override
   protected void onPostCreate(Bundle savedInstanceState){
      super.onPostCreate(savedInstanceState);

      // Trigger the initial hide() shortly after the activity has been
      // created, to briefly hint to the user that UI controls
      // are available.
//      delayedHide(100);
   }

   public void onPlaceholderClick(View view){
   }

   public void onBtnContactSubmit(View view){
   }
}
