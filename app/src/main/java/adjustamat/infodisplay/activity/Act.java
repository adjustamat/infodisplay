package adjustamat.infodisplay.activity;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.os.Bundle;
import android.support.v13.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;

import adjustamat.infodisplay.R;

public class Act extends ActionBarActivity implements MyActivities{

   @Override
   public Context getMyThemedContext(){
      return getSupportActionBar().getThemedContext();
   }

   @Override
   protected void onCreate(Bundle savedInstanceState){
      super.onCreate(savedInstanceState);
      /*
      TODO: bundle in Act:
      fragment index (not needed if everything is in different activities.

       */
      // TODO: actionbar - how delegate to frags?
      setContentView(R.layout.activity);
      FragmentManager fragmentManager = getFragmentManager();
      ViewPager pager = (ViewPager)findViewById(R.id.pager);
      MyFragments myFragments;
      pager.setAdapter(myFragments = new MyFragments(fragmentManager));
//      navdrawer = (Navdrawer)fragmentManager.findFragmentById(R.id.navdrawer);
//      //mTitle = getGroupTitle();
   }

//   @Override
//   public boolean onOptionsItemSelected(MenuItem item){
//      int id = item.getItemId();
//      switch(id){
//         case R.id.action_settings:
//            Navdrawer.showSettings(this);
//            return true;
//         case R.id.action_example:
//            Navdrawer.example(this);
//            return true;
//      }
//      return super.onOptionsItemSelected(item);
//   }
// TODO: can't I move this to ActCreator?
//  R.id.action_add_display    Toast.makeText(this, "btnAdd clicked!", Navdrawer.toastDuration).show();

   /**
    * Create the ActionBar actions for the current fragment.
    * @param menu the ActionBar menu
    * @return true
    */
   @Override
   public boolean onCreateOptionsMenu(Menu menu){
      Menu menu1 = menu;
      // TODO: MANUALLY move this to the fragments. see onPrepareOptionsMenu and onOptionsItemSelected

      /*
      The default implementation populates the menu with standard system menu items.
      These are placed in the Menu.CATEGORY_SYSTEM group so that they will be correctly ordered with application-defined menu items.
      Deriving classes should always call through to the base implementation.
       */
      getMenuInflater().inflate(R.menu.actions, menu);
      return true;
   }

   public void onAttach(int newSection){
      switch(newSection){
         case 0:
            //            mTitle = getString(R.string.title_section1);
            break;
         case 1:
            break;
      }
   }


   // SECTION 1: Main
/*
      ActCreator and ActEditor is one fragment with an exchangable view that swipes in from the right.
      The editor has in the actionBar an Untitled textbox, and all the menu items as displayAsAction="never"

      The display creator has only create, rename, edit, remove.
       in list:
        tapping edits.
        long-tap lifts display-item and starts ordering state,
        or if you leave it under your finger, opens menu.

       */

   // SECTION 2: ActCreator

   /**
    * Per the navigation drawer design guidelines, updates the action bar to show the global app
    * 'context', rather than just what's in the current screen.
    */
//   private void showGlobalContextActionBar(){
//      ActionBar actionBar = getActivity().getActionBar();
//      //actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
//      actionBar.setDisplayShowTitleEnabled(true);
//      actionBar.setTitle(R.string.app_name);
//   }
//   public void restoreActionBar(){
//      ActionBar actionBar = getActionBar();
//      //actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
//      actionBar.setDisplayShowTitleEnabled(true);
//      actionBar.setTitle(mTitle);
//   }

   /**
    * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
    * one of the sections/tabs/pages.
    * The {@link android.support.v4.view.PagerAdapter} that will provide
    * fragments for each of the sections. We use a
    * {@link android.support.v13.app.FragmentPagerAdapter} derivative, which will keep every
    * loaded fragment in memory. If this becomes too memory intensive, it
    * may be best to switch to a
    * {@link android.support.v13.app.FragmentStatePagerAdapter}.
    */
   public class MyFragments extends FragmentPagerAdapter{
      public MyFragments(FragmentManager fm){
         super(fm);
      }

      @Override
      public Fragment getItem(int position){
//         switch(position){
//            case 0:
//               return new Main();
//            case 1:
//               return new ActCreator();
//         }
         return null;
      }

      @Override
      public CharSequence getPageTitle(int position){
//         switch(position){
//            case 0:
               return getString(R.string.app_name); // .toUpperCase(l)
//            case 1:
//               return getString(R.string.title_section2);
//         }
//         return null;
      }

      @Override
      public int getCount(){
         return 2;
      }
   }
}
// v7abdt extends ActionBarDrawerToggle{
// public void onDrawerClosed(View drawerView){
//        super.onDrawerClosed(drawerView);
//        if(!isAdded())
//           return;
//         getActivity().invalidateOptionsMenu(); // calls onCreateOptionsMenu()
// }
//
// public void onDrawerOpened(View drawerView){
//        super.onDrawerOpened(drawerView);
//        if(!isAdded())
//           return;
//        if(!userLearnedDrawer){
//                  userLearnedDrawer = true;
//                  SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getActivity());
//                  sp.edit().putBoolean(userLearnedDrawerKey, true).apply();
//        }
//        getActivity().invalidateOptionsMenu(); // calls onCreateOptionsMenu()
// }
//};