package adjustamat.infodisplay.activity;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import adjustamat.infodisplay.Actions;
import adjustamat.infodisplay.R;

public class ActCreator extends ActionBarActivity{
   @Override
   protected void onCreate(Bundle savedInstanceState){
      super.onCreate(savedInstanceState);
      // BEGIN_INCLUDE (inflate_set_custom_view)
      // Inflate a "Done/Cancel" custom action bar view.
      final LayoutInflater inflater = (LayoutInflater)getSupportActionBar().getThemedContext()
                                                                           .getSystemService(LAYOUT_INFLATER_SERVICE);
      final View customActionBarView = inflater.inflate(R.layout.actionbar_custom_view_done_cancel, null);
      customActionBarView.findViewById(R.id.actionbar_done)
                         .setOnClickListener(new View.OnClickListener(){
                            public void onClick(View v){
                               finish();
                            }
                         });
      customActionBarView.findViewById(R.id.actionbar_cancel)
                         .setOnClickListener(new View.OnClickListener(){
                            public void onClick(View v){
                               finish();
                            }
                         });

      // Show the custom action bar view and hide the normal Home icon and title.
      final ActionBar actionBar = getSupportActionBar();
      actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM,
                                  ActionBar.DISPLAY_SHOW_CUSTOM | ActionBar.DISPLAY_SHOW_HOME );
      actionBar.setCustomView(customActionBarView, new ActionBar.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                                                                              ViewGroup.LayoutParams.MATCH_PARENT));
      // END_INCLUDE (inflate_set_custom_view)
      setContentView(R.layout.activity_creator);
   }

   public void onBtnAddDisplay(View view){
      Actions.addDisplay();
   }
   /*
     > onAttach(Activity) called once the fragment is associated with its activity.
       onCreate(Bundle) called to do initial creation of the fragment.
     > onCreateView(LayoutInflater, ViewGroup, Bundle) creates and returns the view hierarchy associated with the fragment.
       onActivityCreated(Bundle) tells the fragment that its activity has completed its own Activity.onCreate().
       onViewStateRestored(Bundle) tells the fragment that all of the saved state of its view hierarchy has been restored.
       onStart() makes the fragment visible to the user (based on its containing activity being started).
       onResume() makes the fragment interacting with the user (based on its containing activity being resumed).
   ===============================================================================================
       onPause() fragment is no longer interacting with the user either because its activity is being paused or a fragment operation is modifying it in the activity.
       onStop() fragment is no longer visible to the user either because its activity is being stopped or a fragment operation is modifying it in the activity.
       onDestroyView() allows the fragment to clean up resources associated with its View.
       onDestroy() called to do final cleanup of the fragment's state.
       onDetach() called immediately prior to the fragment no longer being associated with its activity.
public static final int INDEX = 1;
@Override
   public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
      return inflater.inflate(R.layout.activity_creator, container, false);
   }

   @Override
   public void onAttach(Activity activity){
      super.onAttach(activity);
      ((Act)activity).onAttach(INDEX);
   }
    */
//   private static final String ARG_SECTION_NUMBER = "section_number";
//   public static ActCreator newInstance(){
//      ActCreator fragment = new ActCreator();
//      Bundle args = new Bundle();
//      args.putInt(ARG_SECTION_NUMBER, sectionNumber);
//      fragment.setArguments(args);
//      return fragment;
//   }
}
