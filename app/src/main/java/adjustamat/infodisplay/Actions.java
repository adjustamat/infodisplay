package adjustamat.infodisplay;
/**
 * Actions.
 */
public class Actions{

   private Actions(){
   }

   public static void addLine(){
   }

   public static void addDisplay(){
   }

   public static void menuSwitch(int id){
      switch(id){
         case R.id.action_
      }<item android:id="@+id/action_nav_settings"
               android:title="@string/nav_settings"
               android:icon="@android:drawable/ic_menu_preferences"
               android:orderInCategory="100"
               app:showAsAction="never"/>
         <group android:id="@+id/acgroup_orderer"
                android:visible="false">
            <item android:id="@+id/action_moveup"
                  android:title="@string/action_moveup"/>
            <item android:id="@+id/action_movedn"
                  android:title="@string/action_movedn"/>
            <item android:id="@+id/action_movetop"
                  android:title="@string/action_movetop"/>
            <item android:id="@+id/action_movebottom"
                  android:title="@string/action_movebottom"/>
            <item android:id="@+id/action_select_none"
                  android:title="@string/action_select_none"/>
         </group>
         <group android:id="@+id/acgroup_creator"
                android:visible="false">
            <item android:id="@+id/action_add_display"
                  android:title="@string/action_add_display"
                  android:icon="@android:drawable/ic_input_add"/>
            <item android:id="@+id/action_del_display"
                  android:title="@string/action_del_display"
                  android:icon="@android:drawable/ic_delete"/>
            <item android:id="@+id/action_order_displays"
                  android:title="@string/action_order_displays"
                  android:icon="@android:drawable/ic_menu_sort_by_size"/>
            <item android:id="@+id/action_name_display"
                  android:title="@string/action_name_display"
                  android:icon="@android:drawable/ic_menu_edit"/>
            <item android:id="@+id/action_edit_display"
                  android:title="@string/action_edit_display"
                  android:icon="@android:drawable/ic_menu_edit"/>
         </group>
         <group android:id="@+id/acgroup_editor"
                android:visible="false">
            <item android:id="@+id/action_add_line"
                  android:title="@string/action_add_line"
                  android:icon="@android:drawable/ic_input_add"/>
            <item android:id="@+id/action_add_line_font_color"
                  android:title="@string/action_add_line_font_color"
                  android:icon="@android:drawable/ic_input_add"/>
            <item android:id="@+id/action_del_line"
                  android:title="@string/action_del_line"
                  android:icon="@android:drawable/ic_delete"/>
            <item android:id="@id/action_del_display"
                  android:title="@string/action_del_display"
                  android:icon="@android:drawable/ic_menu_delete"/>
            <item android:id="@+id/action_cancel_changes"
                  android:title="@string/action_cancel_changes"
                  android:icon="@android:drawable/ic_menu_revert"/>
            <item android:id="@+id/action_done"
                  android:title="@string/action_done"
                  android:icon="@android:drawable/ic_menu_save"/>
            <!-- TODO: find Checkmark-Done icon instead of diskett-save icon. in android downloaded icon-pack? -->
            <item android:id="@+id/action_order_lines"
                  android:title="@string/action_order_lines"
                  android:icon="@android:drawable/ic_menu_sort_by_size"/>
            <!-- TODO: Replace 3 with 1 font-and-color -->
            <!--<group android:id="@+id/actions_textproperties">-->
            <item android:id="@+id/action_font"
                  android:title="@string/action_font"
                  app:showAsAction="ifRoom"
                  android:icon="@android:drawable/ic_menu_sort_alphabetically"/>
            <item android:id="@+id/action_color_fg"
                  android:title="@string/action_color_fg"/>
            <item android:id="@+id/action_color_bg"
                  android:title="@string/action_color_bg"/>
            <!--</group>-->
         </group>
   }
}
